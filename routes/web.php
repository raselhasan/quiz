<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::post('admin-user-login', 'LoginController@login')->name('admin.user.login');
Route::group(['middleware' => ['auth'], 'prefix' => 'admin'], function () {
    Route::group(['middleware' => ['Admin']], function () {
        Route::get('dashboard', 'DashboardController@index')->name('dashboard');
        Route::get('reading-compitision', 'ReadingController@index')->name('admin.reading');
        Route::post('reading-compitision', 'ReadingController@add')->name('admin.reading.add');
        Route::get('add-question/{quiz_id?}', 'QuestionController@index')->name('admin.question.add');
        Route::post('upload-book', 'QuestionController@uploadBook')->name('admin.book.upload');
    });
});
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

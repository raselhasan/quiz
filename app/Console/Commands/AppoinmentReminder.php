<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Mail;
use App\Booking;
use App\Mail\Reminder;

class AppoinmentReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'appointment:reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Appoinment Reminder';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $date = date('Y-m-d');
        $booking1 = Booking::whereDate('send_reminder1','=',$date)->where('reminder',1)->with('user')->get();
        $booking2 = Booking::whereDate('send_reminder2','=',$date)->where('reminder',1)->with('user')->get();
        $booking = $booking1->merge($booking2);
        if(count($booking) > 0){
            foreach ($booking as $key => $booked) {
                Mail::send( new Reminder($booked));
                $up = Booking::find($booked->id);
                if($up->send_reminder1 == date('Y-m-d')){
                    $up->send_reminder1 = null;
                }
                if($up->send_reminder2 == date('Y-m-d')){
                    $up->send_reminder2 = null;
                }
                $up->save();
            }
        }


        
    }
}

<?php

namespace App\Http\Controllers;

use App\Book;
use Illuminate\Http\Request;
use App\Quiz;

class QuestionController extends Controller
{
    public function index($quiz_id = null)
    {
        $data['quiz'] = Quiz::where('id', $quiz_id)->first();
        return view('quiz.admin.question.index', $data);
    }
    public function uploadBook(Request $request)
    {
        $file = $request->file('file');
        $file_name = time() . '.' . $file->getClientOriginalExtension();
        $file->move(public_path('books'), $file_name);
        $book = new Book();
        $book->quiz_id = $request->id;
        $book->title = $request->title;
        $book->language = $request->language;
        $book->book = $file_name;
        $book->save();
    }
}

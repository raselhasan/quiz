<?php

namespace App\Http\Controllers;

use Hash;
use Helper;
use App\User;
use Auth;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function register(Request $request)
    {

        $validator = $this->validate($request, [
            'email' => 'required|unique:users,email'
        ]);

        if(Helper::passwordChecker($request->password) == 'error'){
            
            return redirect()->back()->with('password_error','Password should be more then 7 charecter and one upercase, lowercase, number and special charecter')->withInput($request->all());
        }
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->role = 3;
        $user->status = 1;
        $user->save();

        $credentials = [
            'email' => $request->email,
            'password' => $request->password,
        ];

        if (Auth::attempt($credentials)) {
            if(Auth::user()->role == 3){
                return redirect()->route('customer',['id'=>Auth::user()->id]);
            }else{
                return redirect()->route('dashboard');
            }
        }

    }
}

<?php

namespace App\Http\Controllers;

use App\User;
use Auth;
use Illuminate\Http\Request;
use Hash;
class LoginController extends Controller
{
    public function login(Request $request)
    {
    	$check = User::where('email',$request->email)->where('role',1)->first();
    	if(!$check){
    		return redirect()->back()->withInput($request->input());
    	}
    	$credentials = [
		    'email' => $request->email,
		    'password' => $request->password,
		];

	   	if (Auth::attempt($credentials)) {
            return redirect()->route('dashboard');
	        
	    }else{
	    	return redirect()->back()->withInput($request->input());
	    }
    }
}

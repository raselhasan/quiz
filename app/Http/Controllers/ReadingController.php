<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Quiz;

class ReadingController extends Controller
{
    public function index()
    {
        $data['quizs'] = Quiz::orderBy('id', 'desc')->get();
        return view('quiz.admin.reading.index', $data);
    }
    public function add(Request $request)
    {
        $quiz = new Quiz;
        $quiz->title = $request->title;
        $quiz->start_date = $request->start_date;
        $quiz->start_time = $request->start_time;
        $quiz->end_date = $request->end_date;
        $quiz->end_time = $request->end_time;
        $quiz->price = $request->price;
        $quiz->save();
        return redirect()->back()->with('success', 'Quiz successfully saved!');
    }
}

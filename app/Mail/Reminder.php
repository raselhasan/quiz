<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Reminder extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $data;
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $date_and_time = date('D',strtotime($this->data->date)).' '.date('M, d, Y',strtotime($this->data->date)).', '.$this->data->time;

        $subject = 'REMINDER: You have an appointment on '.$date_and_time.' with '. $this->data->user->name;

        $message  = $this->from('info@globaltaxnyc.com', 'True Medical Care')->view('crm.mail.reminder',['data'=>$this->data,'date_and_time'=>$date_and_time])->subject($subject)->to(trim($this->data->email));
        return $message;
    }
}

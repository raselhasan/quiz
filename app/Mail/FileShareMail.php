<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class FileShareMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $data;
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
         $message  = $this->from('info@globaltaxnyc.com', 'Global Multi Services Inc')->view('crm.mail.file-share-mail',['data'=>$this->data])->subject(auth()->user()->name.' has been shared a file')->to(trim($this->data['email']))->attach($this->data['path']);
        return $message;
    }
}

<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class FileUploadMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $data;
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $message  = $this->from('info@globaltaxnyc.com', 'Global Multi Services Inc')->view('crm.mail.file-upload-mail',['data'=>$this->data])->subject('File Uploaded')->to(trim($this->data['email']));
        return $message;
    }
}

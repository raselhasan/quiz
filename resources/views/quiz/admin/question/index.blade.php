@extends('quiz.admin.layout.layout')

@section('title')
    Add Questions
@endsection

@section('cssfile')
    
@endsection

@section('style')

@endsection

@section('content')
    <!-- Page-header start -->
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-8">
                    <div class="page-header-title">
                        <h5 class="m-b-10">Add Questions</h5>
                        <p class="m-b-0">Lorem Ipsum is simply dummy text of the printing</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <button class="btn waves-effect waves-light btn-primary btn-skew fl-right" data-toggle="modal" data-target="#add-book-modal">Add Book</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Page-header end -->
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="row">
                        @include('quiz.admin.question.quiz-details')
                        @include('quiz.admin.question.books.books')
                        @include('quiz.admin.question.question.add')
                    </div>
                </div>
            </div>
        </div>
    </div>

    
@endsection

@section('jsfile')

@endsection

@section('script')

@endsection                        
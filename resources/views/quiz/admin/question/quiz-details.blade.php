<div class="col-sm-12">
    <div class="card">
        <div class="card-header">
            <h5>Quiz Details</h5>
            <span>Lorem Ipsum is simply dummy text of the printing</span>
        </div>
        <div class="card-block">
            <div class="quiz-dts">
                <p>{{$quiz->title}}</p>
                <p>From {{date('m/d/Y h:i A',strtotime($quiz->start_date.' '.$quiz->start_time))}} - {{date('m/d/Y h:i A',strtotime($quiz->end_date.' '.$quiz->end_time))}}</p>
                @if($quiz->price)
                    <p>${{number_format($quiz->price, 2)}}</p>
                @endif    
            </div>
        </div>
    </div>
</div>
<div class="col-sm-12">
    <div class="card">
        <div class="card-header">
            <h5>Books</h5>
            <span>Lorem Ipsum is simply dummy text of the printing</span>
        </div>
        <div class="card-block">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Book Title</th>
                            <th>Book Language</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @include('quiz.admin.question.books.book-list')
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@include('quiz.admin.question.books.add-book')
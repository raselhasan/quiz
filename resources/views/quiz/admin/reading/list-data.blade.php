@if(count($quizs) > 0)
	@foreach ($quizs as $quiz)
		<tr>
			<td>{{$quiz->title}}</td>
			<td>{{date('m/d/Y', strtotime($quiz->start_date))}}</td>
			<td>{{date('h:i A',strtotime($quiz->start_time))}}</td>
			<td>{{date('m/d/Y', strtotime($quiz->end_date))}}</td>
			<td>{{date('h:i A',strtotime($quiz->end_time))}}</td>
			<td>${{number_format($quiz->price)}}</td>
			<td>
				<a href="{{route('admin.question.add',['quiz_id'=>$quiz->id])}}" class="btn waves-effect waves-dark btn-success btn-outline-success edit-del-btn"><i class="ti-plus"></i></a>
				<button class="btn waves-effect waves-dark btn-info btn-outline-info edit-del-btn"><i class="ti-slice"></i></button>
				<button class="btn waves-effect waves-dark btn-danger btn-outline-danger edit-del-btn"><i class="ti-trash"></i></button>
			</td>
		</tr>
	@endforeach
@endif		
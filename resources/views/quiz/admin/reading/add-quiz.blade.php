<form action="{{route('admin.reading.add')}}" method="post">
    @csrf
    <div class="modal fade" id="add-quiz-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Quiz</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                   <div class="row">
                       <div class="col-md-4">
                            <div class="form-group form-default">
                                <label class="float-label">Quiz Title</label>
                                <input type="text" name="title" class="form-control" required="">
                            </div>
                       </div>
                       <div class="col-md-4">
                            <div class="form-group form-default">
                                <label class="float-label">Start Date</label>
                                <input type="date" name="start_date" class="form-control" required="">
                            </div>
                       </div>
                       <div class="col-md-4">
                            <div class="form-group form-default">
                                <label class="float-label">Start Time</label>
                                <input type="time" name="start_time" class="form-control" required="">
                            </div>
                       </div>
                       <div class="col-md-4">
                            <div class="form-group form-default">
                                <label class="float-label">End Date</label>
                                <input type="date" name="end_date" class="form-control" required="">
                            </div>
                       </div>
                       <div class="col-md-4">
                            <div class="form-group form-default">
                                <label class="float-label">End Time</label>
                                <input type="time" name="end_time" class="form-control" required="">
                            </div>
                       </div>
                       <div class="col-md-4">
                            <div class="form-group form-default">
                                <label class="float-label">Price Money</label>
                                <input type="number" name="price" class="form-control">
                            </div>
                       </div>
                   </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary waves-effect waves-light ">Save changes</button>
                </div>
            </div>
        </div>
    </div>
</form>
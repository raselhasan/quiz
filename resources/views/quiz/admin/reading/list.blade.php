<div class="card">
    <div class="card-header">
        <h5>Reading Compitision</h5>
        <span>Lorem Ipsum is simply dummy text of the printing</span>
    </div>
    <div class="card-block">
        <div class="dt-responsive table-responsive">
            <table id="order-table" class="table table-striped table-bordered nowrap">
                <thead>
                    <tr>
                        <th>Quiz Title</th>
                        <th>Start Date</th>
                        <th>Start Time</th>
                        <th>End Date</th>
                        <th>End Time</th>
                        <th>Price</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @include('quiz.admin.reading.list-data')
                </tbody>
                <tfoot>
                    <tr>
                        <th>Quiz Title</th>
                        <th>Start Date</th>
                        <th>Start Time</th>
                        <th>End Date</th>
                        <th>End Time</th>
                        <th>Price</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
<!doctype html>
<html lang="en-us">
<head>
    <title>Registration</title>
    <link rel="icon" href="{{ asset('crm/favicon-icon.png') }}" type="image/png" sizes="32x32">
     <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"
    rel = "stylesheet">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="{{asset('crm/assets/home/mobile-code.css')}}"> 
    <link rel="stylesheet" type="text/css" href="{{asset('crm/assets/home/style.css')}}"> 
    <link rel="stylesheet" type="text/css" href="{{asset('crm/assets/fonts/fontawesome/css/all.css')}}"> 
    <script src="{{asset('crm/assets/home/jquery.js')}}"></script>
    <script src="{{asset('crm/assets/home/jquery-ui.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.9/js/intlTelInput.js"></script>
    <script src="{{asset('crm/assets/home/custom.js')}}"></script>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous" id="bootstrap-css">
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</head>
<!-- Stylesheet -->
<link rel="stylesheet" href="{{asset('crm/assets/css/main.min3661.css')}}">
<style type="text/css">
    .o-page__card {
        width: 450px;
        margin: 0 auto 3.125rem;
    }
    .error{
        color: red;
    }
    .tooltiptext {
      visibility: hidden;
      width: 300px;
      background-color: white;
      color: black;
      text-align: center;
      border-radius: 6px;
      padding: 5px 0;
      font-size: 16px;

      /* Position the tooltip */
      position: absolute;
      z-index: 1;
    }
    .fa-info-circle:hover .tooltiptext {
      visibility: visible;
    }
</style>
<body class="o-page pt-0">

    @include('crm.home.header')
    <div class="o-page__card o-page--center pb-4">
        <div class="c-card u-mb-xsmall">
            <header class="c-card__header u-pt-large">
                <a class="c-card__icon" href="{{route('home.index')}}">
                    <img src="{{asset('crm/logo/Logo-icon.png')}}" alt="Dashboard UI Kit">
                </a>
                <h1 class="u-h3 u-text-center u-mb-zero">Sign up to Get Started</h1>
            </header>
            
            <form class="c-card__body" method="POST" action="{{ route('user.register') }}">
                @csrf
                <div class="row">
                    @if(Session::has('success'))
                        <div class="col-lg-12">
                            <div class="c-alert c-alert--success">
                                <i class="c-alert__icon fa fa-check-circle"></i> {{Session::get('success')}}
                            </div>
                        </div>
                    @endif
                    <div class="col-lg-12">
                        <div class="c-field u-mb-small">
                            <label class="c-field__label" for="input1">Full Name</label> 
                            <input class="c-input" type="name" id="input1" placeholder="" name="name" value="{{ old('name') }}" required> 
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="c-field u-mb-small">
                            <label class="c-field__label" for="input1">Email</label> 
                            <input class="c-input" type="email" id="input1" placeholder="" name="email" value="{{ old('email') }}" required> 
                            @if($errors->has('email'))
                                <div class="error">{{ $errors->first('email') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="c-field u-mb-small">
                            <label class="c-field__label" for="input2">Password <i class="fa fa-info-circle"><span class="tooltiptext">Password at least 8 charecter 1 upercase, lowercase, number and special charecter</span></i></label> 
                            <input class="c-input" type="password" id="input2" placeholder="" name="password" value="{{old('password')}}" required> 
                            @if(Session::has('password_error'))
                                <div class="error">{{Session::get('password_error')}}</div>
                            @endif 
                        </div>
                    </div>
                </div>
                
                <button class="c-btn c-btn--info c-btn--fullwidth" type="submit">Register</button>
            </form>
        </div>
        <div class="o-line">
            <a class="u-text-mute u-text-small" href="{{route('login')}}">Already have account please login</a>
             <a class="u-text-mute u-text-small" href="{{route('home.index')}}">Back to Home</a>
        </div>
    </div>

    @include('crm.home.footer')

    <!-- Main javascsript -->
    <script src="{{asset('crm/assets/js/main.min3661.js')}}"></script>
    <script src="{{asset('crm/assets/home/header-footer.js')}}"></script>
</body>
</html>